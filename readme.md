![Python](https://img.shields.io/badge/python-v3.6+-blue.svg) ![License](https://img.shields.io/badge/license-MIT-blue.svg)

# Implementation of Digital Image Processing Techniques in Python

The Image Processing Techniques which are relevant in domain are:
* [Image Processing in Spatial Domain](https://glcdn.githack.com/eflaner/image-processing-techniques-in-python/-/raw/master/Basic_Image_Operations_.html)
  - Reading color image & extracting RGBA components.
  - Converting Colour image to Greyscale image
  - Controlling contrast & brightness of image
  - Thresholding, Negative and power-law transforms
* [A simple 2D convolution](https://glcdn.githack.com/eflaner/image-processing-techniques-in-python/-/raw/master/2D_Convolution.html)
* [Histogram Equalization](https://glcdn.githack.com/eflaner/image-processing-techniques-in-python/-/raw/master/Histogram_Equalization.html)
* [Spatial Domain - Image Filtering Techniques](https://glcdn.githack.com/eflaner/image-processing-techniques-in-python/-/raw/master/High_Pass_Filtering_and_Edge_Detection_of_Image.html)
  - Edge detection using
  	- Sobel Operator
  	- Roberts-Cross Operator
  	- Prewitt operator
  - High pass filtering
  	- Second order Laplacian Filter
  	- Unsharp masking
  	- Boost Filtering
* [Fourier Transform of Image](https://glcdn.githack.com/eflaner/image-processing-techniques-in-python/-/raw/master/Discrete_Fourier_Transform_of_Image.html)
* [Frequency Domain - Image Filtering Techniques ](https://glcdn.githack.com/eflaner/image-processing-techniques-in-python/-/raw/master/Image_Filtering_in_Frequency_Domain.html)
  - High Pass & Low Pass
	- Ideal
	- Butterworth
	- Gaussian
* [Image Restoration Techniques ](https://glcdn.githack.com/eflaner/image-processing-techniques-in-python/-/raw/master/Image_Degradation_and_Image_Restoration.html)
	- Gaussian, Arithmetic & Geometric Mean de-noising
	- Triple Round median filtering on S&N noised image
	- Inducing Linear motion & turbulence image
* [Noise filtering Techniques ](https://glcdn.githack.com/eflaner/image-processing-techniques-in-python/-/raw/master/Noise_Filtering.html)
	- Salt & Peppered, Gaussian & Speckle Noise
	- Average, Weighted average, Median, unsharpening & bit plane removal
* [Advanced Transformation Techniques ](https://glcdn.githack.com/eflaner/image-processing-techniques-in-python/-/raw/master/Advanced_Transformation_Techniques.html)
	- Discrete Cosine Transform (DCT)
	- Karhunen-Loeve Transform (KLT)
	- Singular Vector Decomposition (SVD)

There are more Techniques than above specified ones, but these were the ones that I've implemented as part of the 'Digital Image Processing' Laboratory

### ~~Prerequisites~~

The files present in this repo are  html version of `.ipynb` files, which were generated using Jupyter.

### How to simulate
If you don't have Jupyter installation, No Worries, you can still simulate the `.html` file contents using:
* [JupyterLab](https://jupyter.org/try)
* [Binder](https://mybinder.org/)
* [nbviewer](https://nbviewer.jupyter.org/)
* [Google's Colab](https://research.google.com/colaboratory)

## Author

* **Babu P S** - [eflaner](https://gitlab.com/eflaner)

## License

This project is licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.php).

## Acknowledgements

*As a student, I've copied snippets from various authors, as the ultimate aim was to get the desired output. Now i regret, for not making note of authors from whom I've borrowed. I sincerely acknowledge their work and as token of gratitude the work done in this repo is dedicated to all of them.*
